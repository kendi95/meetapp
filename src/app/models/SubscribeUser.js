import { Model, Sequelize } from 'sequelize';

class SubscribeUser extends Model {
    static init(sequelize){
        super.init({
            date: Sequelize.DATE
        }, {
            sequelize
        });

        return this;
    }
    static associate(models){
        this.belongsTo(models.User, { foreignKey: 'user_id', as: 'users' });
        this.belongsTo(models.Meetup, { foreignKey: 'meetup_id', as: 'meetups' });
    }
}

export default SubscribeUser;