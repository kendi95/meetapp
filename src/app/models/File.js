import { Model, Sequelize } from 'sequelize';

class File extends Model{
    static init(sequelize){
        super.init({
            path: Sequelize.STRING,
            name: Sequelize.STRING,
            url: {
                type: Sequelize.VIRTUAL,
                get() {
                    return `http://localhost:3030/uploads/${this.path}`;
                }
            }
        },
        {
            sequelize
        })

        return this;
    }

    static associate(models) {
        this.hasOne(models.Meetup);
    }

}
export default File;