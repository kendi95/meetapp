import { Model, Sequelize } from 'sequelize';
import bcrypt from 'bcryptjs';

class User extends Model{
    static init(sequelize) {
        super.init({
            name: Sequelize.STRING,
            email: Sequelize.STRING,
            password: Sequelize.STRING
        }, {
            sequelize,
        });

        this.addHook('beforeCreate', async (user) => {
            try{
                user.password = await bcrypt.hash(user.password, 10);
                return this;
            }catch(err){
                return err;
            }
            
        });

        this.addHook('beforeUpdate', async (user) => {
            try{
                user.password = await bcrypt.hash(user.password, 10);
                return this;
            }catch(err){
                return err;
            }
        })

        return this;

    }

    static associate(models){
        this.hasMany(models.Meetup);
        this.hasMany(models.SubscribeUser);
    }

    checkPassword(password){
        return bcrypt.compare(password, this.password);
    }
}

export default User;