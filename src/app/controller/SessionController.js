import User from '../models/User';
import * as Yup from 'yup';
import JWT from '../../utils/jwt';

class SessionController {
    async store(req, res) {
        try{
            const schema = Yup.object().shape({
                email: Yup.string().email().required(),
                password: Yup.string().required() 
            });
            if(! (await schema.isValid(req.body))){
                return res.status(400).json({ error: 'Validation fails.' });
            }

            const { email, password } = req.body;
            const user = await User.findOne({where: {email}});
            if(!user){
                return res.status(400).json({error: 'User not found.'});
            }
            if(! (await user.checkPassword(password))){
                return res.status(400).json({ error: 'Password not match.' });
            }
            const token = await JWT.encrypt(user.id);
            const userAuthenticate = {
                name: user.name,
                token
            }

            return res.status(200).json(userAuthenticate);
        }catch(err){
            console.log(err);
        }
        
    }
}

export default new SessionController();