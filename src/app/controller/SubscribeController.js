import SubscribeUser from '../models/SubscribeUser';
import Meetup from '../models/Meetup';
import User from '../models/User';
import { isAfter, isSameHour, isSameDay, getHours, format, ptBR } from 'date-fns';
import Mail from '../../libs/Mail';

class SubscribeController {
    async store(req, res) {
        const meetup = await Meetup.findByPk(req.params.id, {
            include: [
                {
                    model: User,
                    as: 'user',
                    attributes: ['id', 'name', 'email']
                }
            ]
        });
        if(meetup.user_id === req.userId){
            return res.status(400).json({ error: 'You can not subscribe for owner meetups.' });
        }

        if( isAfter(new Date(), meetup.date)){
            return res.status(400).json({ error: 'You can not subscribe for past date of meetups.' });
        }

        const ifExists = await SubscribeUser.findOne({
            where: {user_id: req.userId, meetup_id: req.params.id}
        })
        if(ifExists){
            return res.status(400).json({ error: 'You can not subscribe the same meetups.' });
        }

        const ifExistsAnotherDateAndHours = await SubscribeUser.findAll({
            where: { user_id: req.userId,  meetup_id: req.params.id }
        });

        const { date } = req.body;
        if(! isSameDay(meetup.date, ifExistsAnotherDateAndHours.date)){
            const subscribe = await SubscribeUser.create({
                date, 
                user_id: req.userId, 
                meetup_id: parseInt(req.params.id)
            });

            const subscribeUser = await SubscribeUser.findOne({
                where: {id: subscribe.id},
                include: [
                    {
                        model: User,
                        as: 'users',
                        attributes: ['id', 'name', 'email']
                    }
                ]
            });

            await Mail.sendEmail({
                to: `${meetup.user.name} <${meetup.user.email}>`,
                subject: 'Inscricão criada',
                template: 'creation',
                context: {
                    provider: meetup.user.name,
                    user: subscribeUser.users.name,
                    meetup: meetup.title,
                    address: meetup.location,
                    date: format(
                        subscribe.date,
                        "dd/MMMM/yyyy HH:mm",
                        {
                            locale: ptBR
                        }
                    )
                }
            });

            return res.status(201).json(subscribe);
        }

        if(! isSameHour(getHours(meetup.date), getHours(ifExistsAnotherDateAndHours.date))){
            const subscribe = await SubscribeUser.create({
                date, 
                user_id: req.userId, 
                meetup_id: parseInt(req.params.id)
            });

            const subscribeUser = await SubscribeUser.findOne({
                where: {id: subscribe.id},
                include: [
                    {
                        model: User,
                        as: 'user',
                        attributes: ['id', 'name', 'email']
                    }
                ]
            });

            await Mail.sendEmail({
                to: `${meetup.user.name} <${meetup.user.email}>`,
                subject: 'Inscricão criada',
                template: 'creation',
                context: {
                    provider: meetup.user.name,
                    user: subscribeUser.users.name,
                    meetup: meetup.title,
                    address: meetup.location,
                    date: format(
                        subscribe.date,
                        "'dia' dd 'de' MMMM ', às' H:mm'h'",
                        {
                            locale: ptBR
                        }
                    )
                }
            });

            return res.status(201).json(subscribe);
        }

        return res.status(400).json({error: 'You can not subscribe meetups with same date or hours.'});
    }

    async index(req, res) {
        const subscribes = await SubscribeUser.findAll({
            where: { user_id: req.userId }
        });

        return res.status(200).json(subscribes);
    }
}

export default new SubscribeController();