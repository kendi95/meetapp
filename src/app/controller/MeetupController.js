import * as Yup from 'yup';
import Meetup from '../models/Meetup';
import User from '../models/User';
import File from '../models/File';
import { Op } from 'sequelize';
import { isBefore, isAfter, parseISO, format, startOfDay, endOfDay } from 'date-fns';

class MeetupController {
    async store(req, res) {
        const schema = Yup.object().shape({
            title: Yup.string().required(),
            description: Yup.string().required(),
            location: Yup.string().required(),
            date: Yup.date().required(),
            file_id: Yup.number().required()
        });
        if(! (await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validation fails.'});
        }

        const { title, description, location, date, file_id } = req.body;
        if(! isBefore(new Date(), new Date(date))){
            return res.status(400).json({error: 'You can not create meetups with past date.'});
        }

        const meetup = await Meetup.create({
            title,
            description,
            location,
            date,
            user_id: req.userId,
            file_id
        });

        return res.status(201).json(meetup);
    }

    async update(req, res) {
        const meetup = await Meetup.findByPk(req.params.id);
        if(meetup.user_id !== req.userId){
            return res.status(401).json({error: 'You have not permission to edit this meetup.'});
        }

        if(! isAfter(meetup.date, new Date)){
            return res.status(400).json({ error: 'You can not edit this meetup with past date.' });
        }

        const { title, description, location, date, file_id } = req.body;
        const meetupUpdated = await meetup.update({title, description, location, date, file_id});
        return res.json(meetupUpdated);
    }

    async index(req, res){
        const { date, page = 1 } = req.query;   

        if(date){
            const [year, month, day] = date.split("-");
            var newDate = new Date(year+' '+month+' '+day);
    
            const meetups = await Meetup.findAll({
                where: {
                    date: { [Op.between]: [startOfDay(newDate).getTime(), endOfDay(newDate).getTime()] },
                    user_id: req.userId
                },
                limit: 10,
                offset: (page - 1) * 10,
                include: [
                    {
                        model: User,
                        as: 'user',
                        attributes: ['id', 'name', 'email']
                    }
                ],
                
            });
    
            return res.status(200).json(meetups);
        }

        const meetups = await Meetup.findAll({
            where: {user_id: req.userId},
            attributes: [
                'id',
                'title',
                'description',
                'location',
                'date'
            ],
            include: [
                {
                    model: User,
                    as: 'user',
                    attributes: ['id', 'name', 'email']
                },
                {
                    model: File,
                    as: 'file',
                    attributes: ['id', 'name', 'url', 'path']
                }
            ]
        });
        return res.status(200).json(meetups);
    }

    async destroy(req, res){
        const meetup = await Meetup.findByPk(req.params.id, {
            attributes: ['id', 'title', 'description', 'location', 'date', 'user_id', 'canceled_at']
        });

        if(!meetup){
            return res.status(400).json({ error: "This meetup don't exists." });
        }

        if(meetup.user_id !== req.userId){
            return res.status(401).json({error: 'You have not permission to cancel this meetup.'});
        }

        if(! isAfter(meetup.date, new Date)){
            return res.status(400).json({ error: 'You can not edit this meetup with past date.' });
        }

        await meetup.destroy();
        return res.status(200).json(meetup);
    }

}

export default new MeetupController();