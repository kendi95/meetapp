import * as Yup from 'yup';
import User from '../models/User';

class UserController {

    async store(req, res){
        
        try{
            const schema = Yup.object().shape({
                name: Yup.string().required(),
                email: Yup.string().email().required(),
                password: Yup.string().min(6).required()
            });
            if(!(await schema.isValid(req.body))){
                return res.status(400).json({ error: 'Validation fails!' });
            }

            const { name, email, password } = req.body;
            const user = await User.create({
                name,
                email,
                password
            });
            return res.status(201).json(user);
        }catch(err){
            console.log(err);
        }
        
    }

    async update(req, res) {
        try{
            const schema = Yup.object().shape({
                name: Yup.string().required(),
                email: Yup.string().email().required(),
                password: Yup.string().min(6).required(),
                confirmPassword: Yup.string().required().when('password', (password, field) => 
                    password ? field.required().oneOf([Yup.ref('password')]) : field)
            });
            if(!(await schema.isValid(req.body))){
                return res.status(400).json({ error: 'Validation fails!' });
            }

            const userId = req.userId;
            const { name, email, password } = req.body;
            const user = await User.findByPk(userId);
            if(!user){
                return res.status(400).json({error: 'User not found.'});
            }
            const {id} = await user.update({name, email, password});
            const userUpdated = {
                id, name, email
            }
            return res.status(200).json(userUpdated);

        }catch(err){
            return res.status(500).json(err);
        }
    }

}

export default new UserController();