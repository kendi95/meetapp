import JWT from '../../utils/jwt';

export default async (req, res, next) => {
    try{
        const auth = req.headers.authorization;
        if(!auth){
            return res.status(400).json({error: 'Token is not provided.'});
        }

        const [, token] = await auth.split(' ');
        const { id } = await JWT.decoded(token);
        req.userId = id;
        next();
    }catch(err){
        return res.status(400).json({error: 'Token invalid.'});
    }
    
}