import mailConfig from '../config/mail';
import nodemailer from 'nodemailer';
import handlebars from 'express-handlebars';
import mailHandlebars from 'nodemailer-express-handlebars';
import { resolve } from 'path';

class Mail {
    constructor(){
        const { host, port, auth, secure } = mailConfig;
        this.transporter = nodemailer.createTransport({
            host, port, secure, auth: auth.user ? auth : null
        });

        this.configureTemplate();
    }

    sendEmail(message) {
		return this.transporter.sendMail({
			...mailConfig.default,
			...message
		});
	}

    configureTemplate(){
        const viewPath = resolve(__dirname, '..', 'app', 'views', 'emails');
        this.transporter.use(
            'compile',
            mailHandlebars({
                viewEngine: handlebars.create({
                    layoutsDir: resolve(viewPath, 'layouts'),
                    partialsDir: resolve(viewPath, 'partials'),
                    defaultLayout: 'default',
                    extname: '.handlebars'
                }),
                viewPath,
                extname: '.handlebars'
            })
        );
    }
}

export default new Mail();