import jwt from 'jsonwebtoken';
import authConfig from '../config/auth';

class JWT {
    async encrypt(id) {
        return await jwt.sign({id}, authConfig.secret, {
            expiresIn: authConfig.expiresIn
        });
    }
    async decoded(token) {
        return await jwt.decode(token);
    }
}

export default new JWT();