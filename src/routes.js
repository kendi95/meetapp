import Router from 'express';

import multer from 'multer'
import multerConfig from './config/multer';

const routes = new Router();
const upload = multer(multerConfig);

import middlewares from './app/middlewares/auth';

import UserController from './app/controller/UserController';
import SessionController from './app/controller/SessionController';
import UploadsController from './app/controller/UploadsController';
import MeetupController from './app/controller/MeetupController';
import SubscribeController from './app/controller/SubscribeController';
import SubscribeUser from './app/models/SubscribeUser';

routes.post('/users', UserController.store);
routes.post('/sessions', SessionController.store);

routes.use(middlewares);
routes.put('/users', UserController.update);

routes.post('/uploads', upload.single('file'), UploadsController.store);

routes.post('/meetups', MeetupController.store);
routes.put('/meetups/:id', MeetupController.update);
routes.delete('/meetups/:id', MeetupController.destroy);
routes.get('/meetups', MeetupController.index);

routes.post('/meetups/:id/subscribes', SubscribeController.store);
routes.get('/subscribes', SubscribeController.index);

export default routes;